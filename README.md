#  Single-page handout listing of topics that can be incorporated in a README file. 

The document provides **an overview of all possible topics which could be mentioned in a README for data, for code, or for both**, as far as the authors are aware of. Which topics actually to include in a specific README file depends on the dataset and/or code, institutional context etcetera. The topics in bold are the very bare minimum (from FAIR data perspective); in general, the intention is to add as much relevant information as possible to a README file while keeping it readable.

This document is created for the internal *Workshop FAIR data and data reuse for ESG researchers*, provided in October 2022, at Wageningen University and Research, Netherlands. This workshop was **financed** by the Wageningen Data Competence Center. Although missing the context of the workshop, the provided materials are also usable stand-alone.

**Authors**: [Cindy Quik](https://orcid.org/0000-0002-7112-0195) and [Luc Steinbuch](https://orcid.org/0000-0001-6484-0920) (Contact)


**Keywords**: README, data management, FAIR, code, software

**URL** to this project: https://git.wur.nl/FAIR-data-for-ESG/creating-a-readme

**DOI** of all workshop materials: https://doi.org/10.4121/21399975


This project consists of **three files**: <tt>Possible_topics_README.vsdx</tt>, created with Microsoft&reg; Visio&reg; Version 2202, the resulting pdf/A (1.6): <tt>Possible_topics_README.pdf</tt>, and a <tt>README_template.txt<tt> based on the other documents. 


This git project is **licensed** under [CC BY-SA-NC 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

Among others, we **used for inspiration** (all accessed in June until October 2022):

- https://data.4tu.nl/info/fileadmin/user_upload/Documenten/Guidelines_for_creating_a_README_file.pdf 
- https://data.research.cornell.edu/content/readme  
- https://datamanagement.hms.harvard.edu/collect-analyze/documentation-metadata/readme-files 
- https://edepot.wur.nl/534147
- https://tilburgsciencehub.com/building-blocks/store-and-document-your-data/document-data/readme-best-practices/ 
- https://uwyo.libguides.com/ld.php?content_id=61896490 
- https://www.makeareadme.com/
- https://zenodo.org/record/4058972#.YzwhjNjP2Um
 
